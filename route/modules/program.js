const express = require("express");
const controller = require("../../controller/program");
const router = express.Router();
const authMiddleware = require("../../middleware/auth");

router.get("/", [authMiddleware], controller.index);
router.get(
    "/detail", [authMiddleware, controller.validasi("detail")],
    controller.detailProgram
);
router.get("/kaprodi", [authMiddleware], controller.kaprodi);

module.exports = router;