module.exports = {
    getProgram: (conn) => {
        return conn
            .execute("select p.program, p.keterangan, p.lama_studi, p.kode_epsbed, p.gelar, p.gelar_inggris, p.program_ijazah, p.program_ijazah_singkat, p.program_skpi, p.semester_maksimal, p.program_transkrip from program p left join program_studi ps on p.nomor = ps.program where ps.kepala=344")
            //.execute("SELECT * FROM PROGRAM p order by program ")
            .then((res) =>
                res.rows.map((item) => {
                    return {
                        nomor: item[0],
                        program: item[1],
                        keterangan: item[2],
                        lama_studi: item[3],
                        kode_epsbed: item[4],
                        gelar: item[5],
                        gelar_inggris: item[6],
                        program_ijazah: item[7],
                        program_ijazah_singkat: item[8],
                        program_skpi: item[9],
                        semester_maksimal: item[10],
                        program_transkrip: item[11],
                    };
                })
            )
            .catch((err) => {
                console.log(err);
                return [];
            });
    },
    detailProgram: (conn, nomor) => {
        return conn
            .execute("SELECT * FROM PROGRAM p WHERE p.NOMOR =:0 ", [nomor])
            .then((res) =>
                res.rows.map((item) => {
                    return {
                        nomor: item[0],
                        program: item[1],
                        keterangan: item[2],
                        lama_studi: item[3],
                        kode_epsbed: item[4],
                        gelar: item[5],
                        gelar_inggris: item[6],
                        program_ijazah: item[7],
                        program_ijazah_singkat: item[8],
                        program_skpi: item[9],
                        semester_maksimal: item[10],
                        program_transkrip: item[11],
                    };
                })
            )
            .catch((err) => {
                console.log(err);
                return [];
            });
    },
};